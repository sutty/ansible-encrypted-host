# Instalar un host seguro para Docker

## Features

* Instala Archlinux

  **TODO:** Parabolizar

* Cifra los discos
* Configura SSH temprano para poder desbloquear remotamente
* Configura firewall
* Hace backups del particionado

## Bootstrap

Antes de empezar hay que:

* Crear una contraseña para el cifrado remoto
* Iniciar un instalador de Archlinux/Parabola en el provedor de VPS
* Entrar por consola VNC
* Configurar contraseña para root
* Configurar red
* Configurar hostname
* Generar llaves de SSH en formato PEM para poder convertirlas con
  TinySSH
* Iniciar sshd
* Copiar llaves SSH

  **Importante:** Tiene que haber al menos una llave SSH RSA o ECDSA
  para que dropbear la pueda reconocer.

En cada VPS:

```bash
echo "root:una contraseña temporal" | chpasswd
ip link set ens3 up
ip address add 1.2.3.4/24 dev ens3
ip route add default via 1.2.3.1 dev ens3
rm /etc/ssh/ssh_host_*
ssh-keygen -A -m PEM
systemctl restart sshd
hostnamectl set-hostname el.nombre.de.dominio.completo
```

Localmente:

```bash
ssh-copy-id -i ~/.ssh/id_ed25519.pub root@1.2.3.4
```

## Configurar

Editar las variables en `config.yml` donde haga falta.

¡También hay que crear un `vault.yml` cifrado y no olvidarse la
contraseña!

```bash
ansible-vault create vault.yml
```

```yaml
---
cryptsetup:
  # el nombre de cada servidor, esto permite tener varios servidores con
  # distinta configuración.
  hostname:
    # el modo luks es necesario para los servidores instalados con luks
    # (una versión anterior de este playbook)
    mode: plain # o luks
    key: Contraseña para decifrar el disco
    # obtener los mejores resultados de `cryptsetup benchmark`
    cipher: ''
    hash: ''
    key_size: ''
    # la partición de root
    root: /dev/vda3
    # el nombre que tendrá el abrirse en /dev/mapper/
    mapper: root
  # se pueden agregar otros hostnames a partir de aquí
# la contraseña de root no la usamos mucho, todas las conexiones se
# hacen por ssh
root:
  hostname: Una contraseña para root
```

## Instalar

**Atención:** Todavía no es seguro correr el mismo Ansible en un
servidor ya instalado.  Una vez instalado ya no hay que usar este
Playbook.

Agregar el host al inventario de Ansible:

```ini
el.nombre.de.dominio.completo

[encrypted_hosts]
el.nombre.de.dominio.completo
```

Correr el instalador:

```bash
make install hosts=el.nombre.de.dominio.completo
```

## Desbloquear el disco

Cada vez que se reinicie el servidor, hay que desbloquear el disco:

```bash
make unblock
```

Esta acción se conecta por SSH al servidor, ingresa la contraseña
y continúa con el inicio del sistema.  Si se corre mientras el servidor
está iniciado no pasa nada.
